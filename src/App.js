import React, { useState, useEffect, useRef } from 'react';
import { TextField, Button, List, ListItem, ListItemText, Container, Typography, Box } from '@mui/material';

function App() {
  const [messages, setMessages] = useState([]);
  const [input, setInput] = useState('');
  const [currentUser, setCurrentUser] = useState('User1');  // Default to User1
  const messagesEndRef = useRef(null);

  // Load messages from local storage and set up storage event listener
  useEffect(() => {
    const loadMessages = () => {
      const storedMessages = JSON.parse(localStorage.getItem('chatMessages')) || [];
      setMessages(storedMessages);
    };

    loadMessages();  // Load messages initially
    window.addEventListener('storage', loadMessages);  // Listen for storage changes

    return () => {
      window.removeEventListener('storage', loadMessages);  // Cleanup listener
    };
  }, []);

  const sendMessage = () => {
    if (input && currentUser) {
      const newMessage = { user: currentUser, text: input };
      const updatedMessages = [...messages, newMessage];
      setMessages(updatedMessages);
      localStorage.setItem('chatMessages', JSON.stringify(updatedMessages));  // Update local storage
      setInput('');
    }
  };

  const switchUser = (user) => {
    setCurrentUser(user);
  };

  useEffect(() => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  return (
    <Container sx={{ display: 'flex', marginTop: 4 }}>
      <Box sx={{ width: 200, marginRight: 2 }}>
        <Typography variant="h6">Users</Typography>
        <List component="nav">
          <ListItem button selected={currentUser === 'User1'} onClick={() => switchUser('User1')}>
            <ListItemText primary="User1" />
          </ListItem>
          <ListItem button selected={currentUser === 'User2'} onClick={() => switchUser('User2')}>
            <ListItemText primary="User2" />
          </ListItem>
        </List>
      </Box>
      <Box sx={{ flex: 1 }}>
        <Typography variant="h4">Chat as {currentUser}</Typography>
        <List sx={{ maxHeight: 300, overflow: 'auto', mb: 2 }}>
          {messages.map((message, index) => (
            <ListItem key={index}>
              <ListItemText primary={`${message.user}: ${message.text}`} />
            </ListItem>
          ))}
          <div ref={messagesEndRef} />
        </List>
        <TextField
          label="Type your message..."
          variant="outlined"
          fullWidth
          value={input}
          onChange={e => setInput(e.target.value)}
          margin="normal"
        />
        <Button onClick={sendMessage} variant="contained" color="primary">
          Send
        </Button>
      </Box>
    </Container>
  );
}

export default App;
