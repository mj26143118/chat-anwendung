// src/components/ChatRoom.js

import React, { useState, useEffect, useRef } from 'react';

function ChatRoom() {
    const [messages, setMessages] = useState([]);
    const [input, setInput] = useState('');
    const messagesEndRef = useRef(null);

    const scrollToBottom = () => {
        messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
    }

    useEffect(() => {
        scrollToBottom();
    }, [messages]);

    const sendMessage = (e) => {
        e.preventDefault();
        if (!input.trim()) return;
        const newMessages = [...messages, { id: messages.length + 1, text: input }];
        setMessages(newMessages);
        setInput('');
    };

    return (
        <div className="chat-room">
            <div className="messages">
                {messages.map(message => (
                    <div key={message.id} className="message">{message.text}</div>
                ))}
                <div ref={messagesEndRef} />
            </div>
            <form onSubmit={sendMessage}>
                <input
                    value={input}
                    onChange={e => setInput(e.target.value)}
                    type="text"
                    placeholder="Type a message..."
                    autoFocus
                />
                <button type="submit">Send</button>
            </form>
        </div>
    );
}

export default ChatRoom;
