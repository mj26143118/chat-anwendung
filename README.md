Testing the Application
To test the chat functionality across multiple users:

Open Multiple Tabs or Windows
Open two instances of your browser.
Navigate to http://localhost:3000 in each tab or window.
Set Different Users
In the first tab/window, select "User1" from the left sidebar.
In the second tab/window, select "User2".
Send Messages
Type a message in one tab and click "Send".
Switch to the other tab to see the message appear there as well.
